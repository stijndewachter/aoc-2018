package me.dewachter.aoc2018

fun day2() {
    val input = getInputFromFile("day2")
    printResults(2, part1(input), part2(input))
}

private fun part1(input: List<String>): String {
    var containsTwoCount = 0
    var containsThreeCount = 0

    input.forEach { id ->
        val letters = id.split("").dropWhile { it == "" }
        val counts = ArrayList<Int>()

        letters.distinct().forEach { letter -> counts.add(letters.count { it == letter }) }

        if (counts.contains(2)) containsTwoCount++
        if (counts.contains(3)) containsThreeCount++
    }

    return (containsTwoCount * containsThreeCount).toString()
}

private fun part2(input: List<String>): String {
    val inputList = ArrayList<CharArray>()
    input.forEach { inputList.add(it.toCharArray()) }
    val mutableInputList = ArrayList(inputList)

    inputList.forEach { currentWord ->
        mutableInputList.remove(currentWord)
        mutableInputList.forEach {
            var differenceChar = 0
            var differences = 0
            for (j in it.indices) {
                if (currentWord[j] != it[j]) {
                    differences++
                    differenceChar = j
                }
            }

            if (differences == 1) {
                return currentWord.joinToString("").removeRange(differenceChar..differenceChar)
            }
        }
    }

    return "Matching IDs not found."
}