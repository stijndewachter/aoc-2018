package me.dewachter.aoc2018

import java.io.File

const val RESET = "\u001B[0m"
const val RED_UL = "\u001B[4;31m"
const val GREEN = "\u001B[32m"
const val YELLOW = "\u001B[33m"

fun getInputFromFile(name: String): List<String> {
    val file = File("resources/$name.txt")
    return file.readText().split("\n")
}

fun printResults(day: Int, resultOne: String, resultTwo: String) {
    println("${RED_UL}Day $day $RESET")
    println("$GREEN- Part 1: $YELLOW$resultOne $RESET")
    println("$GREEN- Part 2: $YELLOW$resultTwo $RESET")
}