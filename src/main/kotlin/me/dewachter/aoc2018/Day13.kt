package me.dewachter.aoc2018

import me.dewachter.aoc2018.Cart.Choice.*
import me.dewachter.aoc2018.Cart.Direction.*
import me.dewachter.aoc2018.Rail.Type
import me.dewachter.aoc2018.Rail.Type.*
import java.util.*

class Day13 {
    private var input: List<String> = getInputFromFile("day13")
    private lateinit var network: LinkedList<LinkedList<Rail>>
    private var ticks = 1

    fun solve() {
        printResults(13, part1(), part2())
    }

    private fun part1(): String {
        network = readDiagram()
        var (status, message) = moveAllCarts(true)
        while (status) {
            val moveData = moveAllCarts(true)
            status = moveData.first
            message = moveData.second
        }
        return message
    }

    private fun part2(): String {
        network = readDiagram()
        var (_, message) = moveAllCarts(false)
        while (message.isBlank()) {
            val moveData = moveAllCarts(false)
            message = moveData.second
        }

        for (y in network.indices) {
            for (x in network[y].indices) {
                if (network[y][x].cart != null) {
                    return "$x,$y"
                }
            }
        }

        return "Cart not found"
    }

    private fun moveAllCarts(returnOnCrash: Boolean): Pair<Boolean, String> {
        ticks++
        val movedCarts = ArrayList<Cart>()
        for (y in network.indices) {
            loop@ for (x in network[y].indices) {
                val currentRail = network[y][x]
                val cart = when (currentRail.cart) {
                    null -> continue@loop
                    else -> network[y][x].cart!!
                }

                when (movedCarts.contains(cart)) {
                    true -> continue@loop
                    else -> movedCarts.add(cart)
                }

                val (newX, newY) = when (cart.direction) {
                    NORTH -> Pair(x, y - 1)
                    EAST -> Pair(x + 1, y)
                    SOUTH -> Pair(x, y + 1)
                    WEST -> Pair(x - 1, y)
                }
                val newRail = network[newY][newX]

                if (newRail.cart != null) {
                    movedCarts.remove(currentRail.cart!!)
                    movedCarts.remove(newRail.cart!!)
                    currentRail.cart = null
                    newRail.cart = null
                    if (returnOnCrash) {
                        return Pair(false, "$newX,$newY")
                    } else {
                        continue@loop
                    }
                }

                cart.direction = when (newRail.type) {
                    INTERSECTION -> when (cart.nextChoice) {
                        LEFT -> {
                            cart.nextChoice = STRAIGHT
                            when (cart.direction) {
                                NORTH -> WEST
                                EAST -> NORTH
                                SOUTH -> EAST
                                WEST -> SOUTH
                            }
                        }
                        STRAIGHT -> {
                            cart.nextChoice = RIGHT
                            cart.direction
                        }
                        RIGHT -> {
                            cart.nextChoice = LEFT
                            when (cart.direction) {
                                NORTH -> EAST
                                EAST -> SOUTH
                                SOUTH -> WEST
                                WEST -> NORTH
                            }
                        }
                    }
                    EAST_TO_WEST, NORTH_TO_SOUTH -> cart.direction
                    NORTH_TO_EAST -> when (cart.direction) {
                        SOUTH -> EAST
                        WEST -> NORTH
                        else -> return Pair(false, "Cart entered impossible track N2E")
                    }
                    EAST_TO_SOUTH -> when (cart.direction) {
                        WEST -> SOUTH
                        NORTH -> EAST
                        else -> return Pair(false, "Cart entered impossible track E2S")
                    }
                    SOUTH_TO_WEST -> when (cart.direction) {
                        NORTH -> WEST
                        EAST -> SOUTH
                        else -> return Pair(false, "Cart entered impossible track S2W")

                    }
                    WEST_TO_NORTH -> when (cart.direction) {
                        EAST -> NORTH
                        SOUTH -> WEST
                        else -> return Pair(false, "Cart entered impossible track W2N")
                    }
                    else -> return Pair(false, "Cart entered impossible track NR")
                }

                newRail.cart = cart
                currentRail.cart = null
            }
        }
        return when (movedCarts.size) {
            1 -> Pair(true, "1 cart remaining")
            else -> Pair(true, "")
        }
    }

    private fun readDiagram(): LinkedList<LinkedList<Rail>> {
        val network = LinkedList<LinkedList<Rail>>()

        input.forEach { line ->
            val networkLine = LinkedList<Rail>()
            var lastChar = ' '
            line.forEach { char ->
                val railType: Type = when (char) {
                    '+' -> INTERSECTION

                    '-', '>', '<' -> EAST_TO_WEST
                    '|', 'v', '^' -> NORTH_TO_SOUTH

                    '/' -> when (lastChar in listOf('-', '>', '<', '+')) {
                        true -> WEST_TO_NORTH
                        else -> EAST_TO_SOUTH
                    }
                    '\\' -> when (lastChar in listOf('-', '>', '<', '+')) {
                        true -> SOUTH_TO_WEST
                        else -> NORTH_TO_EAST
                    }

                    else -> NO_RAIL
                }

                val cart: Cart? = when (char) {
                    '^' -> Cart(NORTH)
                    '>' -> Cart(EAST)
                    'v' -> Cart(SOUTH)
                    '<' -> Cart(WEST)
                    else -> null
                }

                lastChar = char
                networkLine.add(Rail(railType, cart))
            }
            network.add(networkLine)
        }

        return network
    }
}

class Rail(val type: Type, var cart: Cart?) {
    enum class Type {
        NO_RAIL,
        INTERSECTION,
        EAST_TO_WEST,
        NORTH_TO_SOUTH,
        NORTH_TO_EAST,
        EAST_TO_SOUTH,
        SOUTH_TO_WEST,
        WEST_TO_NORTH;
    }
}

class Cart(var direction: Direction) {
    var nextChoice = LEFT

    enum class Direction {
        NORTH, EAST, SOUTH, WEST
    }

    enum class Choice {
        LEFT, STRAIGHT, RIGHT
    }
}


