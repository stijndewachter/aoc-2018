package me.dewachter.aoc2018

fun day3() {
    val input = getInputFromFile("day3")
    printResults(3, part1(input), part2(input))
}

private fun part1(input: List<String>): String {
    val matrix = Array(1000) { IntArray(1000) { 0 } }
    input.forEach { claim ->
        val claimData = parseClaim(claim)
        for (i in claimData.second.first until claimData.second.first + claimData.third.first) {
            for (j in claimData.second.second until claimData.second.second + claimData.third.second) {
                matrix[i][j]++
            }
        }
    }

    var doubleClaimed = 0
    for (i in matrix.indices) {
        for (j in matrix[i].indices) {
            if (matrix[i][j] > 1) {
                doubleClaimed++
            }
        }
    }
    return doubleClaimed.toString()
}

private fun part2(input: List<String>): String {
    val matrix = Array(1000) { IntArray(1000) { 0 } }
    val claimIdList = MutableList(input.size) { it + 1 }
    input.forEach { claim ->
        val claimData = parseClaim(claim)
        for (i in claimData.second.first until claimData.second.first + claimData.third.first) {
            for (j in claimData.second.second until claimData.second.second + claimData.third.second) {
                when (matrix[i][j]){
                     0 -> matrix[i][j] = claimData.first
                    else -> {
                        claimIdList.remove(claimData.first)
                        claimIdList.remove(matrix[i][j])
                    }
                }
            }
        }
    }

    return claimIdList[0].toString()
}

private fun parseClaim(claimData: String): Triple<Int, Pair<Int, Int>, Pair<Int, Int>> {
    val splitData = claimData.split(' ')
    val claimId = Integer.parseInt(splitData[0].removePrefix("#"))

    val splitCoords = splitData[2].split(',')
    val claimStartX = Integer.parseInt(splitCoords[0])
    val claimStartZ = Integer.parseInt(splitCoords[1].removeSuffix(":"))

    val splitSpan = splitData[3].split('x')
    val claimSpanX = Integer.parseInt(splitSpan[0])
    val claimSpanZ = Integer.parseInt(splitSpan[1])
    return Triple(claimId, Pair(claimStartX, claimStartZ), Pair(claimSpanX, claimSpanZ))
}