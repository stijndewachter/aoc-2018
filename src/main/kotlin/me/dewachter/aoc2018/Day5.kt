package me.dewachter.aoc2018

import java.util.Collections.min
import kotlin.math.min

fun day5() {
    val input = getInputFromFile("day5")
    printResults(5, part1(input), part2(input))
}

private fun part1(input: List<String>): String {
    val polymer = collapsePolymer(input.first())

    return polymer.length.toString()
}

private fun part2(input: List<String>): String {
    val basePolymer = input.first()
    var shortestPolymer = basePolymer.length

    for (unit in 'a'..'z') {
        val polymerToCollapse = basePolymer.replace("${unit.toUpperCase()}", "").replace("${unit.toLowerCase()}", "")
        shortestPolymer = min(shortestPolymer, collapsePolymer(polymerToCollapse).length)
    }

    return shortestPolymer.toString()
}

private fun collapsePolymer(polymer: String): String {
    var polymer = polymer
    do {
        val previousPolymer = polymer
        for (letter in 'a'..'z') {
            val lower = letter.toLowerCase()
            val upper = letter.toUpperCase()
            polymer = polymer.replace("$lower$upper", "")
            polymer = polymer.replace("$upper$lower", "")
        }
    } while (polymer != previousPolymer)

    return polymer
}