package me.dewachter.aoc2018

import java.util.*
import kotlin.math.abs

class Day9 {
    private var input: List<String> = getInputFromFile("day9")

    fun solve() {
        printResults(9, part1(), part2())
    }

    private fun part1(): String {
        val (playerCount, maxMarbleWorth) = parseInput()
        return MarbleGame(playerCount, maxMarbleWorth).play().getHighScore().toString()
    }

    private fun part2(): String {
        val (playerCount, maxMarbleWorth) = parseInput()
        return MarbleGame(playerCount, maxMarbleWorth * 100).play().getHighScore().toString()
    }

    private fun parseInput(): Pair<Int, Int> {
        val split = input[0].split(" ")
        return Pair(Integer.parseInt(split[0]), Integer.parseInt(split[6]))
    }
}

private class MarbleGame(private val playerCount: Int, private val maxMarbleWorth: Int) {
    val playerScores = HashMap<Int, Long>()

    fun play(): MarbleGame {
        val marbleHistory = CircleDeque<Long>()
        marbleHistory.addFirst(0)
        for (marble in 1..maxMarbleWorth) {
            if (marble % 23 == 0) {
                marbleHistory.rotate(-7)
                val score = marble + marbleHistory.pop()
                playerScores[marble % playerCount] = playerScores[marble % playerCount]?.plus(score) ?: score
            } else {
                marbleHistory.rotate(2)
                marbleHistory.addLast(marble.toLong())
            }
        }
        return this
    }

    fun getHighScore(): Long {
        return playerScores.maxBy { it.value }?.value ?: 0
    }
}

private class CircleDeque<T> : ArrayDeque<T>() {
    fun rotate(positions: Int) {
        if (positions == 0) {
            return
        }
        if (positions > 0) {
            for (i in 0 until positions) {
                val t: T = this.removeLast()
                this.addFirst(t)
            }
        } else {
            for (i in 0 until abs(positions) - 1) {
                val t: T = this.removeFirst()
                this.addLast(t)
            }
        }
    }
}