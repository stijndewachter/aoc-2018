package me.dewachter.aoc2018

fun day4() {
    val input = getInputFromFile("day4")
    printResults(4, part1(input), part2(input))
}

private fun part1(input: List<String>): String {
    val guardHistory = generateGuardHistory(input)

    var sleepiestGuard = 0
    var totalSleepTime = 0
    guardHistory.forEach { (guardId, history) ->
        if (history.sum() > totalSleepTime) {
            sleepiestGuard = guardId
            totalSleepTime = history.sum()
        }
    }

    val sleepiestMinute = guardHistory[sleepiestGuard]!!.max()?.let { guardHistory[sleepiestGuard]!!.indexOf(it) }

    return (sleepiestGuard * sleepiestMinute!!).toString()
}

private fun part2(input: List<String>): String {
    val guardHistory = generateGuardHistory(input)

    var mostConsistentGuard = 0
    var highestSleepTime = 0
    guardHistory.forEach { (guardId, history) ->
        if (history.max()!! > highestSleepTime) {
            mostConsistentGuard = guardId
            highestSleepTime = history.max()!!
        }
    }

    val sleepiestMinute = guardHistory[mostConsistentGuard]!!.max()?.let { guardHistory[mostConsistentGuard]!!.indexOf(it) }

    return (mostConsistentGuard * sleepiestMinute!!).toString()
}

private fun generateGuardHistory(input: List<String>): HashMap<Int, IntArray> {
    val guardHistory = HashMap<Int, IntArray>()
    val sortedInput = input.sorted()
    var currentGuard = 0
    var lastSleepTime = 0
    sortedInput.forEach {
        when {
            it.contains("Guard") -> {
                val guardId = parseGuardOnDuty(it)
                if (!guardHistory.containsKey(guardId)) {
                    guardHistory[guardId] = IntArray(60) { 0 }
                }
                currentGuard = guardId
            }
            it.contains("falls asleep") -> lastSleepTime = parseWakeOrSleepTime(it)
            it.contains("wakes up") -> {
                val awakeTime = parseWakeOrSleepTime(it)
                for (i in lastSleepTime until awakeTime) {
                    guardHistory[currentGuard]!![i]++
                }
            }
            else -> println("Unreadable line: $it")
        }
    }

    return guardHistory
}

private fun parseGuardOnDuty(line: String): Int {
    return Integer.parseInt(line.substringAfter('#').substringBefore(' '))
}

private fun parseWakeOrSleepTime(line: String): Int {
    return Integer.parseInt(line.substringAfter(':').substringBefore(']'))
}