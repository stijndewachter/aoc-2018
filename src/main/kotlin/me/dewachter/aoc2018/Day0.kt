package me.dewachter.aoc2018

class Day0 {
    private var input: List<String> = getInputFromFile("day0")

    fun solve() {
        printResults(0, part1(), part2())
    }

    private fun part1(): String {
        return ""
    }

    private fun part2(): String {
        return ""
    }
}