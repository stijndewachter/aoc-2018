package me.dewachter.aoc2018

import java.lang.Integer.min

class Day11 {
    private var input: List<String> = getInputFromFile("day11")
    private val grid = Grid(input[0].toInt())

    fun solve() {
        grid.calculate()
        printResults(11, part1(), part2())
    }

    private fun part1(): String {
        var highestLevel = Int.MIN_VALUE
        var highestCoord = Pair(0, 0)
        for (y in 0..297) {
            for (x in 0..297) {
                var level = 0
                for (yd in 0..2) {
                    for (xd in 0..2) {
                        level += grid.grid[y + yd][x + xd]
                    }
                }
                if (level > highestLevel) {
                    highestLevel = level
                    highestCoord = Pair(x + 1, y + 1)
                }
            }
        }
        return "${highestCoord.first},${highestCoord.second}"
    }

    private fun part2(): String {
        var highestLevel = Int.MIN_VALUE
        var highestCoord = Triple(0, 0, 0)
        for (y in 0..299) {
            for (x in 0..299) {
                for (ad in 0..min(299 - x, 299 - y)) {
                    var level = 0
                    for (yd in 0..ad) {
                        for (xd in 0..ad) {
                            level += grid.grid[y + yd][x + xd]
                        }
                    }

                    if (level > highestLevel) {
                        highestLevel = level
                        highestCoord = Triple(x + 1, y + 1, ad + 1)
                    }
                }
            }
        }
        return "${highestCoord.first},${highestCoord.second},${highestCoord.third}"
    }
}

class Grid(private val serial: Int) {
    val grid = Array(300) { IntArray(300) { 0 } }

    fun calculate() {
        for (y in 1..300) {
            for (x in 1..300) {
                val rackId = x + 10
                var level = (((rackId * y) + serial) * rackId)
                level = when (level < 100) {
                    true -> 0
                    else -> level.toString().split("").dropLast(3).last().toInt() - 5
                }
                grid[y - 1][x - 1] = level
            }
        }
    }
}