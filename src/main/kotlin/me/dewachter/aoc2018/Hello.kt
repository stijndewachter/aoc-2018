package me.dewachter.aoc2018

fun main(args: Array<String>) {
    println("\u001B[1;41;32m=== Advent of Code 2018 Solutions ===\u001B[0m")
//    day1()
//    day2()
//    day3()
//    day4()
//    day5()
//    day6()
//    day7()
//    Day8().solve()
//    Day9().solve()
//    Day10().solve()
//    Day11().solve()
//    Day12().solve()
    Day13().solve()
}

