package me.dewachter.aoc2018

import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.LinkedHashMap

fun day7() {
    val input = getInputFromFile("day7")
    printResults(7, part1(input), part2(input))
}

private fun part1(input: List<String>): String {
    return calculateProcessOrderAndTime(input, 1).first
}

private fun part2(input: List<String>): String {
    return calculateProcessOrderAndTime(input, 5).second.toString()
}

private fun calculateProcessOrderAndTime(input: List<String>, workers: Int): Pair<String, AtomicInteger> {
    val stepSequence = generateStepSequence(input)

    val allSteps = LinkedList<Char>()
    allSteps.addAll('A'..'Z')
    val availableSteps = findFirstSteps(stepSequence).toMutableList()
    val stepsInProgress = LinkedHashMap<Char, AtomicInteger>()
    val completedSteps = LinkedList<Char>()

    val currentTime = AtomicInteger(-1)
    while (completedSteps.size != allSteps.size) {
        currentTime.incrementAndGet()

        // Check which steps are complete
        stepsInProgress.forEach { (step, timeRemaining) ->
            if (timeRemaining.get() == 0) {
                completedSteps.add(step)
            }
        }

        completedSteps.forEach { step -> stepsInProgress.remove(step) }

        // Check which steps are available
        stepSequence.forEach { (step, dependencies) ->
            if (!availableSteps.contains(step)
                && !stepsInProgress.containsKey(step)
                && completedSteps.containsAll(dependencies)
                && !completedSteps.contains(step)
            ) {
                availableSteps.add(step)
            }
        }

        // Tick in progress tasks
        if (stepsInProgress.size > 0) {
            allSteps.forEach { step ->
                if (stepsInProgress.containsKey(step)) {
                    stepsInProgress[step]!!.decrementAndGet()
                }
            }
        }

        // Start tasks
        availableSteps.sort()
        for (step in availableSteps) {
            if (stepsInProgress.size < workers) {
                val processTime = 60 + allSteps.indexOf(step)
                stepsInProgress[step] = AtomicInteger(processTime)
            }
        }

        availableSteps.removeAll(stepsInProgress.keys)
    }

    return Pair(completedSteps.joinToString(""), currentTime)
}

fun generateStepSequence(input: List<String>): Map<Char, ArrayList<Char>> {
    val stepSequence = HashMap<Char, ArrayList<Char>>()

    input.forEach {
        val (dependency, step) = parseLine(it)
        if (!stepSequence.containsKey(step)) {
            stepSequence[step] = ArrayList()
        }
        stepSequence[step]!!.add(dependency)
    }
    return stepSequence
}

private fun findFirstSteps(input: Map<Char, ArrayList<Char>>): List<Char> {
    val firstSteps = ArrayList<Char>()
    val secondSteps = ArrayList<Char>()
    input.forEach { (seconds, firsts) ->
        firstSteps.addAll(firsts)
        secondSteps.add(seconds)
    }
    firstSteps.distinct()
    firstSteps.removeAll(secondSteps)
    return firstSteps.sorted().distinct()
}

private fun parseLine(line: String): Pair<Char, Char> {
    val trimmedLine = line.substringAfter("Step ").substringBefore(" can begin.").toCharArray()
    return Pair(trimmedLine.first(), trimmedLine.last())
}