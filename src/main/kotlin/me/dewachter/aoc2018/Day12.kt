package me.dewachter.aoc2018

import java.util.*

class Day12 {
    private var input: List<String> = getInputFromFile("day12")

    fun solve() {
        printResults(12, part1(), part2())
    }

    private fun part1(): String {
        return getPotSum(20).toString()
    }

    private fun part2(): String {
        val sumAt1000 = getPotSum(1000)
        val deltaPerGeneration = sumAt1000 - getPotSum(999)
        return (sumAt1000 + ((50000000000L - 1000) * deltaPerGeneration)).toString()
    }

    private fun getPotSum(generations: Int): Int {
        var (wall, growth) = parseInput()
        var zero = 0
        for (i in 0 until generations) {
            zero += 3
            wall = ".....$wall..."
            var newWall = ""

            for (j in 2 until wall.length - 2) {
                newWall += when {
                    growth.contains(wall.substring(j - 2, j + 3)) -> "#"
                    else -> "."
                }
            }

            wall = newWall
        }

        var sum = 0
        for ((i, s) in wall.toCharArray().withIndex()) {
            if (s == '#') {
                sum += i - zero
            }
        }

        return sum
    }

    private fun parseInput(): Pair<String, List<String>> {
        val initialState = input[0].split(" ").last()
        val growth = LinkedList<String>()

        input.drop(2).forEach {
            if (it.endsWith('#')) {
                growth.add(it.split(" ").first())
            }
        }

        return Pair(initialState, growth)
    }
}