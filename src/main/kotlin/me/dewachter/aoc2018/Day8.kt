package me.dewachter.aoc2018

import java.util.*
import kotlin.collections.ArrayList

class Day8 {
    private var input: List<String> = getInputFromFile("day8")

    private val rootNode = NodeFinder(parseInput(input)).separateNodes().rootNode

    fun solve() {
        printResults(8, part1(), part2())
    }

    private fun part1(): String {
        return rootNode.getMetaSum().toString()
    }

    private fun part2(): String {
        return rootNode.getMetaValue().toString()
    }

    private fun parseInput(lines: List<String>): MutableList<Int> {
        return lines[0].split(" ").map { Integer.parseInt(it) }.toCollection(ArrayList())
    }
}

private class NodeFinder(input: MutableList<Int>) {
    companion object {
        lateinit var input: MutableList<Int>
    }

    var rootNode: Node

    init {
        Companion.input = input
        val childCount = input.first()
        input.removeAt(0)
        val metaCount = input.first()
        input.removeAt(0)
        rootNode = Node(childCount, metaCount)
    }


    fun separateNodes(): NodeFinder {
        for (childNode in 0 until rootNode.childCount) {
            rootNode.children.add(NodeFinder(input).separateNodes().rootNode)
        }

        for (metadata in 0 until rootNode.metaCount) {
            rootNode.meta.add(input.first())
            input.removeAt(0)
        }

        return this
    }

    internal class Node(val childCount: Int, val metaCount: Int) {
        val children = LinkedList<Node>()
        val meta = LinkedList<Int>()

        fun getMetaSum(): Int {
            return meta.sum() + children.sumBy { it.getMetaSum() }
        }

        fun getMetaValue(): Int {
            return when (childCount) {
                0 -> meta.sum()
                else -> meta.sumBy { children.elementAtOrNull(it - 1)?.getMetaValue() ?: 0 }
            }
        }
    }
}