package me.dewachter.aoc2018

import kotlin.math.abs

class Day10 {
    private var input: List<String> = getInputFromFile("day10")

    fun solve() {
        printResults(10, part1(), part2())
    }

    private val constellation = findMessageInStars()
    private fun findMessageInStars(): Constellation {
        val constellation = ConstellationCreator.create(input)

        do {
            val previousBoundingBoxSize = constellation.getBoundingBoxSize()
            constellation.move()
            val newBoundingBox = constellation.getBoundingBoxSize()
        } while (newBoundingBox < previousBoundingBoxSize)

        constellation.reverseMove()

        return constellation
    }

    private fun part1(): String {
        constellation.plot()
        return "See above for message"
    }

    private fun part2(): String {
        return constellation.totalMoves.toString()
    }
}

object ConstellationCreator {
    fun create(input: List<String>): Constellation {
        val stars = ArrayList<Star>()
        input.forEach { stars.add(parseAndCreateStar(it)) }
        return Constellation(stars)
    }

    private fun parseAndCreateStar(line: String): Star {
        val strippedLine = line
            .replace("position=< ", "")
            .replace("position=<", "")
            .replace("> velocity=< ", " ")
            .replace("> velocity=<", " ")
            .replace(">", "")
            .replace(",  ", " ")
            .replace(", ", " ")
            .split(" ")

        return Star(
            Integer.parseInt(strippedLine[0]), Integer.parseInt(strippedLine[1]),
            Integer.parseInt(strippedLine[2]), Integer.parseInt(strippedLine[3])
        )
    }
}

class Constellation(private val stars: ArrayList<Star>) {
    var totalMoves = 0

    fun plot() {
        val (minMaxX, minMaxZ) = getBoundingBox()
        val (minX, maxX) = minMaxX
        val (minZ, maxZ) = minMaxZ

        for (z in minZ..maxZ) {
            for (x in minX..maxX) {
                when (hasStarAt(x, z)) {
                    true -> print("#")
                    else -> print(" ")
                }
            }
            println()
        }
    }

    private fun hasStarAt(x: Int, z: Int): Boolean {
        return stars.any { (it.x == x && it.z == z) }
    }

    fun move() {
        stars.forEach { it.move() }
        totalMoves++
    }

    fun getBoundingBoxSize(): Int {
        val (x, z) = getBoundingBox()
        return abs(x.second - x.first) + abs(z.second - z.first)
    }

    private fun getBoundingBox(): Pair<Pair<Int, Int>, Pair<Int, Int>> {
        var minX = Int.MAX_VALUE
        var maxX = Int.MIN_VALUE
        var minZ = Int.MAX_VALUE
        var maxZ = Int.MIN_VALUE
        stars.forEach {
            if (it.x < minX) {
                minX = it.x
            }
            if (it.x > maxX) {
                maxX = it.x
            }
            if (it.z < minZ) {
                minZ = it.z
            }
            if (it.z > maxZ) {
                maxZ = it.z
            }
        }

        return Pair(Pair(minX, maxX), Pair(minZ, maxZ))
    }

    fun reverseMove() {
        stars.forEach { it.reverseMove() }
        totalMoves--
    }
}

class Star(var x: Int, var z: Int, private val xv: Int, private val zv: Int) {
    fun move() {
        x += xv
        z += zv
    }

    fun reverseMove() {
        x -= xv
        z -= zv
    }
}

