package me.dewachter.aoc2018

import kotlin.math.abs

fun day6() {
    val input = getInputFromFile("day6")
    printResults(6, part1(input), part2(input))
}

private fun part1(input: List<String>): String {
    val (minMaxX, minMaxZ) = findMinMax(input)
    val (minX, maxX) = minMaxX
    val (minZ, maxZ) = minMaxZ
    val leaderboards = HashMap<String, Int>()
    for (x in minX..maxX) {
        for (z in minZ..maxZ) {
            var closestCoordinate = Pair("0,0", Integer.MAX_VALUE)
            input.forEach {
                val coordinate = splitCoordinate(it)
                val distance = abs(x - coordinate.first) + abs(z - coordinate.second)
                if (distance < closestCoordinate.second) {
                    closestCoordinate = Pair(it, distance)
                } else if (distance == closestCoordinate.second) {
                    closestCoordinate = Pair("Equal", distance)
                }
            }
            if (x == minX || x == maxX || z == minZ || z == maxZ) {
                leaderboards[closestCoordinate.first] = Int.MIN_VALUE
            } else if (closestCoordinate.first != "Equal") {
                leaderboards[closestCoordinate.first] = (leaderboards[closestCoordinate.first] ?: 0) + 1
            }
        }
    }
    return leaderboards.maxBy { it.value }!!.value.toString()
}

private fun part2(input: List<String>): String {
    val (minMaxX, minMaxZ) = findMinMax(input)
    val (minX, maxX) = minMaxX
    val (minZ, maxZ) = minMaxZ
    var area = 0

    for (x in minX..maxX) {
        for (z in minZ..maxZ) {
            var distanceSum = 0
            input.forEach {
                val coordinate = splitCoordinate(it)
                distanceSum += abs (x - coordinate.first) + abs(z - coordinate.second)
            }
            if (distanceSum < 10000) {
                area++
            }
        }
    }

    return area.toString()
}

private fun findMinMax(input: List<String>): Pair<Pair<Int, Int>, Pair<Int, Int>> {
    val listX = ArrayList<Int>()
    val listZ = ArrayList<Int>()

    input.forEach {
        val split = splitCoordinate(it)
        listX.add(split.first)
        listZ.add(split.second)
    }

    return Pair(Pair(listX.min()!!, listX.max()!!), Pair(listZ.min()!!, listZ.max()!!))
}

private fun splitCoordinate(line: String): Pair<Int, Int> {
    val split = line.split(", ")
    return Pair(Integer.parseInt(split[0]), Integer.parseInt(split[1]))
}
