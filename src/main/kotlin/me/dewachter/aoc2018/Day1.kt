package me.dewachter.aoc2018

fun day1() {
    val input = getInputFromFile("day1")
    printResults(1, part1(input), part2(input))
}

private fun part1(input: List<String>): String {
    var frequency = 0;
    input.forEach {
        frequency += Integer.parseInt(it)
    }
    return frequency.toString()
}

private fun part2(input: List<String>): String {
    val history = ArrayList<Int>()
    var frequency = 0;
    while (true) {
        input.forEach {
            frequency += Integer.parseInt(it)

            when (history.contains(frequency)) {
                true -> return frequency.toString()
                else -> history.add(frequency)
            }
        }
    }
}